
const $emailField = document.getElementById("email-field");

const $emailLabel = document.getElementById("email-label");

const $emailError = document.getElementById("email-error");

function validateEmail() {
    $value = $emailField.value;

    const isEmailFilled = $value.length > 0;
    const isEmailValid = $value.match(/^[A-Za-z\._\-0-9]*[@][A-Za-z]*[\.][a-z]{2,4}$/);

    setEmailLabelPosition(isEmailFilled, isEmailValid);
    setError(isEmailValid);
}

function setEmailLabelPosition(isEmailFilled, isEmailValid)
{
    if (isEmailFilled) {
        
        $emailLabel.style.bottom = "45px";
    } else {
        $emailLabel.style.bottom = "20px";
    }

    if (isEmailValid) {
        $emailField.style.borderBottomColor = "green";
    }
    else {
        $emailField.style.borderBottomColor = "red";
    }
}

function setError(isEmailValid)
{
    if (isEmailValid) {
        $emailError.classList.add("hidden");
    } else {
        $emailError.classList.remove("hidden");
    }
}